<?php

drupal_add_css(path_to_theme() . "/reset.css");
drupal_add_css(path_to_theme() . "/generic.css");

function larepublique_regions() {
  return array(
    'sidebar' => t('left sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}