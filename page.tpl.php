<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <style type="text/css" media="print"> @import url(<?php print $directory ?>/print.css); </style>
    <!--[if IE]>
      <style type="text/css" media="all"> @import url(<?php print $directory ?>/ie.css); </style>
      <![endif]-->
    <?php print $scripts ?>
  </head>

  <body>

    <div id="container" class="clear-block">

      <?php if ($header): ?>
        <div id="header" class="typographic">
          <?php print $header ?>
        </div>
      <?php endif ?>

      <div id="top">
        <?php if ($site_name | $logo): ?>
          <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo ?>" alt="<?php print $site_name ?>" />
            <?php endif ?>
          </a>

          <div id="site-name" class="clear-block">
                                                                                            <?php if (!$title) { ?><h1><?php } ?>
            <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>">
              <?php print $site_name ?>
            </a>
                                                                                            <?php if (!$title) { ?></h1><?php } ?>
            <?php if ($site_slogan): ?>
              <?php print "<div id=\"slogan\">($site_slogan)</div>" ?>
            <?php endif ?>
          </div>
        <?php endif ?>

        <?php if ($primary_links): ?>
          <div id="primary-links" class="clear-block">
            <?php print theme('links', $primary_links) ?>
          </div>
        <?php endif ?>
      </div>

      <div id="content">
        <?php if ($title): ?>
          <div id="title">
            <h1><?php print $title; ?></h1>
          </div>
        <?php endif; ?>
        <div id="content-inner" class="typographic">
          <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
          <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
          <?php if ($mission): ?><div class="mission"><?php print $mission; ?></div><?php endif; ?>
          <?php print $help; ?>
          <?php print $messages; ?>
          <?php print $content; ?>
          <?php print $feed_icons; ?>
          <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
        </div>
      </div>

      <?php if ($sidebar | $search_box): ?>
        <div id="sidebar">
          <div class="typographic">
            <?php if ($search_box): ?>
              <div id="search-box">
                <?php print $search_box ?>
              </div>
            <?php endif; ?>
            <?php print $sidebar ?>
          </div>
        </div>
      <?php endif ?>

      <?php if ($footer_message): ?>
        <div id="footer">
          <?php print $footer_message; ?><br/>
          <?php print t("Theme"); ?>: <?php print t("@theme by !designer", array("@theme" => "La Republique", "!designer" => '<a href="http://josesanmartin.com/">José San Martin</a>')); ?>
        </div>
      <?php endif ?>
    </div>

    <?php print $closure ?>
  </body>
</html>